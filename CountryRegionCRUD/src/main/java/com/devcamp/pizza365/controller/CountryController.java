package com.devcamp.pizza365.controller;

import com.devcamp.pizza365.model.CCountry;
import com.devcamp.pizza365.repository.CountryRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
public class CountryController {
	@Autowired
	private CountryRepository countryRepository;

	@CrossOrigin
	@PostMapping("/countries")
	public ResponseEntity<Object> createCountry(@RequestBody CCountry cCountry) {
		try {
			CCountry newRole = new CCountry();
			newRole.setCountryName(cCountry.getCountryName());
			newRole.setCountryCode(cCountry.getCountryCode());
			newRole.setRegions(cCountry.getRegions());
			CCountry savedRole = countryRepository.save(newRole);
			return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed to Create specified Country: "+e.getCause().getCause().getMessage());
		}
	}

	@CrossOrigin
	@PutMapping("/countries/{countryid}")
	public ResponseEntity<Object> updateCountry(@PathVariable("countryid") Long countryid, @RequestBody CCountry cCountry) {
		Optional<CCountry> countryData = countryRepository.findById(countryid);
		if (countryData.isPresent()) {
			CCountry newCountry = countryData.get();
			newCountry.setCountryName(cCountry.getCountryName());
			newCountry.setCountryCode(cCountry.getCountryCode());
			newCountry.setRegions(cCountry.getRegions());
			CCountry savedCountry = countryRepository.save(newCountry);
			return new ResponseEntity<>(savedCountry, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@CrossOrigin
	@DeleteMapping("/countries/{countryid}")
	public ResponseEntity<Object> deleteCountryById(@PathVariable Long countryid) {
		try {
			countryRepository.deleteById(countryid);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@GetMapping("/countries/count")
	public long countCountry() {
		return countryRepository.count();
	}
	
	/**
	 * Viết method kiểm tra country có trong CSDL hay không: checkCountryById() sử dụng hàm existsById() của CountryRepository
	 * @param countryid
	 * @return
	 */	
	@CrossOrigin
	@GetMapping("/countries/check/{countryid}")
	public boolean checkCountryById(@PathVariable Long countryid) {
		return countryRepository.existsById(countryid);
	}

	/**
	 * Tìm country có chứa giá trị trong country code 
	 * @param code
	 * @return
	 */
	@CrossOrigin
	@GetMapping("/country/containing-code/{code}")
	public List<CCountry> getCountryByContainingCode(@PathVariable String code) {
		return countryRepository.findByCountryCodeContaining(code);
	}

	@CrossOrigin
	@GetMapping("/country/details/{id}")
	public CCountry getCountryById(@PathVariable Long id) {
		if (countryRepository.findById(id).isPresent())
			return countryRepository.findById(id).get();
		else
			return null;
	}

	@CrossOrigin
	@GetMapping("/countries")
	public List<CCountry> getAllCountry() {
		return countryRepository.findAll();
	}

	@CrossOrigin
	@GetMapping("/country1")
	public CCountry getCountryByCountryCode(@RequestParam(value="countryCode") String countryCode) {
		return countryRepository.findByCountryCode(countryCode);
	}
	
	@CrossOrigin
	@GetMapping("/country2")
	public List<CCountry> getCountryByCountryName(@RequestParam(value="countryName") String countryName) {
		return countryRepository.findByCountryName(countryName);
	}
	
	@CrossOrigin
	@GetMapping("/country3")
	public CCountry getCountryByRegionCode(@RequestParam(value="regionCode") String regionCode) {
		return countryRepository.findByRegionsRegionCode(regionCode);
	}
	
	@CrossOrigin
	@GetMapping("/country4")
	public List<CCountry> getCountryByRegionName(@RequestParam(value="regionName") String regionName) {
		return countryRepository.findByRegionsRegionName(regionName);
	}		
}
