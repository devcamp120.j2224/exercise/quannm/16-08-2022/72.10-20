package com.devcamp.pizza365.controller;

import com.devcamp.pizza365.model.CCountry;
import com.devcamp.pizza365.model.CRegion;
import com.devcamp.pizza365.repository.CountryRepository;
import com.devcamp.pizza365.repository.RegionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
public class RegionController {
	@Autowired
	private RegionRepository regionRepository;
	
	@Autowired
	private CountryRepository countryRepository;
	
	@CrossOrigin
	@PostMapping("/regions/{countryId}")
	public ResponseEntity<Object> createRegion(@PathVariable("countryId") Long countryId, @RequestBody CRegion cRegion) {
		try {
			Optional<CCountry> countryData = countryRepository.findById(countryId);
			if (countryData.isPresent()) {
			CRegion newRole = new CRegion();
			newRole.setRegionName(cRegion.getRegionName());
			newRole.setRegionCode(cRegion.getRegionCode());
			newRole.setCountry(cRegion.getCountry());
			
			CCountry _country = countryData.get();
			newRole.setCountry(_country);
			newRole.setCountryName(_country.getCountryName());
			
			CRegion savedRole = regionRepository.save(newRole);
			return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
			}
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed to Create specified Region: "+e.getCause().getCause().getMessage());
		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	@CrossOrigin
	@PutMapping("/regions/{regionId}")
	public ResponseEntity<Object> updateRegion(@PathVariable("regionId") Long regionId, @RequestBody CRegion cRegion) {
		Optional<CRegion> regionData = regionRepository.findById(regionId);
		if (regionData.isPresent()) {
			CRegion newRegion = regionData.get();
			newRegion.setRegionName(cRegion.getRegionName());
			newRegion.setRegionCode(cRegion.getRegionCode());
			CRegion savedRegion = regionRepository.save(newRegion);
			return new ResponseEntity<>(savedRegion, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@CrossOrigin
	@DeleteMapping("/regions/{regionId}")
	public ResponseEntity<Object> deleteRegionById(@PathVariable Long regionId) {
		try {
			regionRepository.deleteById(regionId);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@GetMapping("/regions/{regionId}")
	public CRegion getRegionById(@PathVariable Long regionId) {
		if (regionRepository.findById(regionId).isPresent())
			return regionRepository.findById(regionId).get();
		else
			return null;
	}


	@CrossOrigin
	@GetMapping("/regions")
	public List<CRegion> getAllRegion() {
		return regionRepository.findAll();
	}

	@CrossOrigin
	@GetMapping("/countries/{countryId}/regions")
    public List <CRegion> getRegionsByCountryId(@PathVariable(value = "countryId") Long countryId) {
        return regionRepository.findByCountryId(countryId);
    }
	
	@CrossOrigin
	@GetMapping("/countries/regions")
    public List <CRegion> countRegionByCountryCode(@RequestParam(value = "countryCode") String countryCode) {
        return regionRepository.findByCountryCountryCode(countryCode);
    }

	@CrossOrigin
	@GetMapping("/countries/{countryId}/regions/{regionId}")
    public Optional<CRegion> getRegionByRegionAndCountry(@PathVariable(value = "countryId") Long countryId,@PathVariable(value = "regionId") Long regionId) {
        return regionRepository.findByIdAndCountryId(regionId,countryId);
    }

	@CrossOrigin
	@GetMapping("/countries/{countryId}/count")
    public int countRegionByCountryId(@PathVariable(value = "countryId") Long countryId) {
        return regionRepository.findByCountryId(countryId).size();
    }
	
	@CrossOrigin
	@GetMapping("/regions/check/{regionId}")
	public boolean checkRegionById(@PathVariable Long regionId) {
		return regionRepository.existsById(regionId);
	}		
}

